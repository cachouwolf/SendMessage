DROP DATABASE IF EXISTS SendMessage;
CREATE DATABASE IF NOT EXISTS SendMessage DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE SendMessage;

DROP TABLE IF EXISTS message;
CREATE TABLE IF NOT EXISTS message (
    id int(11) unsigned NOT NULL AUTO_INCREMENT,
    object varchar(200) NOT NULL,
    message varchar(200) NOT NULL,
    author varchar(200) NOT NULL,
    dateEdit DATE NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

INSERT INTO message
(object, message, author, dateEdit)
VALUES
('foo', 'bar', 'foo.Mozart', '2017-01-01'),
('lorem', 'ipsum', 'Madeleine', '2017-01-01'),
('toto', 'titi', 'Gros Minet', '2017-01-01');
