<?php

// importation de classes
use Michelf\Markdown;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// chargement de l'autoloading
require_once __DIR__.'/../vendor/autoload.php';

// gestion des erreurs
ErrorHandler::register();
ExceptionHandler::register();

function getMessage($app, $id) {
    $sql = "SELECT * FROM message WHERE id = ?";

    try {
        $message = $app['db']->fetchAssoc($sql, [(int) $id]);
    } catch (Exception $e) {
        $myException = new StdClass();
        $myException->error = true;
        $myException->message = $e->getMessage();
        $myException->code = $e->getCode();

        return $app->json($myException, 500);
    }

    if (!$message) {
        $myException = new StdClass();
        $myException->error = true;
        $myException->message = 'Vous devez fournir un id valide';
        $myException->code = 0;

        return $app->json($myException, 400);
    }

    return $app->json($message);
}

// création d'une nouvelle appli
$app = new Silex\Application();

// activation du mode déboggage
$app['debug'] = true;

// gestion des erreurs
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    $myException = new StdClass();
    $myException->error = true;
    $myException->message = $e->getMessage();
    $myException->code = $code;

    return $app->json($myException, 500);
});

// transformation automatique de données json dans le corps de la requête en tableau php
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});


// autorisation des connexions ajax
$app->register(new JDesrosiers\Silex\Provider\CorsServiceProvider(), []);

// connexion à la base de données
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'SendMessage',
        'user'      => 'cdujardin',
        'password'  => 'Wosyy6LwwlDO0MzY',
        'charset'   => 'utf8',
    ),
));

// home du site
$app->get('/', function() {
    // @todo utiliser twig plutôt que du html en dur
    $htmlHead = '<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title></title>
  </head>
  <body>
';

    $htmlTail = '  </body>
</html>
';

    // lecture du fichier README.md
    $text = file_get_contents('../README.md');

    // transformation du markdonw en html
    $html = Markdown::defaultTransform($text);

    return $htmlHead . $html . $htmlTail;
});

// liste des messages
$app->get('/messages', function() use ($app) {
    $sql = "SELECT * FROM message";

    try {
        $messages = $app['db']->fetchAll($sql);
    } catch(Exception $e) {
        $myException = new StdClass();
        $myException->error = true;
        $myException->message = $e->getMessage();
        $myException->code = $e->getCode();

        return $app->json($myException, 500);
    }

    return $app->json($messages);
});

// détails d'un message
$app->get('/messages/{id}', function($id) use ($app) {
    return getMessage($app, $id);
});

// création d'un nouveau message
$app->post('/messages/', function(Request $request) use ($app) {
    $object = $request->get('object');
    $message = $request->get('message');
    $author = $request->get('author');
    $dateEdit = $request->get('dateEdit');


    $myException = new StdClass();
    $myException->message = [];


    if (isset($myException->error)) {
        return $app->json($myException, 400);
    }

    try {
        $app['db']->insert('message', [
            'object' => $object,
            'message' => $message,
            'author' => $author,
            'dateEdit' => $dateEdit,

        ]);

        $lastId = $app['db']->lastInsertId();
    } catch(Exception $e) {
        $myException->error = true;
        $myException->message = $e->getMessage();
        $myException->code = $e->getCode();

        return $app->json($myException, 500);
    }

    return getMessage($app, $lastId);
});

// modification d'un message
$app->put('/messages/{id}', function(Request $request, $id) use ($app) {
    $object = $request->get('object');
    $message = $request->get('message');
    $author = $request->get('author');
    $dateEdit = $request->get('dateEdit');


    $app['db']->update('user', [
         'object' => $object,
            'message' => $message,
            'author' => $author,
            'dateEdit' => $dateEdit,
    ], [
        'id' => $id,
    ]);

    return getMessager($app, $id);
});

// suppression d'un message
$app->delete('/messages/{id}', function($id) use ($app) {
    $resultat = $app['db']->delete('message', [
        'id' => (int) $id,
    ]);

    if ($resultat) {
        $return = 204;
    } else {
        $return = 500;
    }

    return new Response('', $return);
});

// autorisation des connexion ajax
$app["cors-enabled"]($app);

// démarrage de l'appli
$app->run();
